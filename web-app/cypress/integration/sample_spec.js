// Test d'exemple par défaut :
describe('The Traffic web site home page', () => {
    it('successfully loads', () => {
        cy.visit('/')
    })
})

describe('Scenario 1', () => {
    it('test configuration page', () => {
        cy.visit('/')
        cy.contains('Configuration').click()
        cy.url().should('include','configuration')
    })
})

describe('Scenario 2', () => {
    it('there is 28 segment in config page', () => {
        cy.visit('/')
        cy.get('a[href="#configuration"]').click();
        cy.get('form[data-kind="segment"]').should('have.length', 28);
    })
})

describe('Scenario 3', () => {
    it('there is zero car', () => {
        cy.get("table").contains('No vehicle available');
    })
})

describe('Scenario 4', () => {
    it('update speed of segment 5 to 30', () => {
        cy.get('input[name="speed"][form="segment-5"]').clear().type('30');
        cy.get('form[id="segment-5"] >button').click();
        cy.get('div.modal-footer button').click();
    })
})

describe('Scenario 5', () => {
    it('update round about', () => {
        cy.get('input[name="capacity"][form="roundabout-31"]').clear().type('4')
        cy.get('input[name="duration"][form="roundabout-31"]').clear().type('15')
        cy.get('form[name="roundabout-31"] >button').click()
        cy.get('div.modal-footer button').click();
    })
})

describe('Scenario 6', () => {
    it('update trafficLight 29', () => {
        cy.get('input[name="orangeDuration"][form="trafficlight-29"]').clear().type('4')
        cy.get('input[name="greenDuration"][form="trafficlight-29"]').clear().type('40')
        cy.get('input[name="nextPassageDuration"][form="trafficlight-29"]').clear().type('8')
        cy.get('form[name="trafficlight-29"] >button').click()
        cy.get('div.modal-footer button').click();
    })
})

describe('Scenario 7', () => {
    it('add 3 vehicles', () => {
        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('50')
        cy.get('form[name="addVehicle"] >button').click()
        cy.get('div.modal-footer button').click();

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('19')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('8')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('200')
        cy.get('form[name="addVehicle"] >button').click()
        cy.get('div.modal-footer button').click();

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('27')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('2')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('150')
        cy.get('form[name="addVehicle"] >button').click()
        cy.get('div.modal-footer button').click();
    })
})

describe('Scenario 8', () => {
    it('all vehicles are stopped and simulation of 120s', () => {
        cy.contains('Simulation').click()

        cy.get('div.col-md-7 > table > tbody > tr:nth-child(1) > th').eq('5').should('contain', 'block')
        cy.get('div.col-md-7 > table > tbody > tr:nth-child(2) > th').eq('5').should('contain', 'block')
        cy.get('div.col-md-7 > table > tbody > tr:nth-child(3) > th').eq('5').should('contain', 'block')
        cy.get('input[name="time"][form="runNetwork"]').clear().type('120')
        cy.get('form[name="runNetwork"] >button').click() // launch a simulation
        cy.wait(5000)
        cy.get('div.col-md-7 > table > tbody > tr:nth-child(3) > th').eq('5').contains('play_circle_filled').should('exist')
        cy.get('div.col-md-7 > table > tbody > tr:nth-child(2) > th').eq('5').contains('block').should('exist')
        cy.get('div.col-md-7 > table > tbody > tr:nth-child(1) > th').eq('5').contains('block').should('exist')
    })
})

describe('Scenario 9', () => {
    it('launch a new simulation of 500s', () => {
        //refresh page
        cy.visit('/')
        cy.get('a[href="#configuration"]').click();

        //test that there is no vehicles
        cy.get('div.col-md-8 > table').contains('No vehicle available');

        //add 3 vehicles
        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('50')
        cy.get('form[name="addVehicle"] >button').click()
        cy.get('div.modal-footer button').click();

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('19')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('8')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('200')
        cy.get('form[name="addVehicle"] >button').click()
        cy.get('div.modal-footer button').click();

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('27')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('2')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('150')
        cy.get('form[name="addVehicle"] >button').click()
        cy.get('div.modal-footer button').click();

        //launch the simulation
        cy.contains('Simulation').click()
        cy.get('input[name="time"][form="runNetwork"]').clear().type('500')
        cy.get('form[name="runNetwork"] >button').click()
        cy.pause()

        // test that all vehicles are stopped
        cy.get('div.col-md-7 > table > tbody > tr:nth-child(1) > th').eq('5').should('contain', 'block')
        cy.get('div.col-md-7 > table > tbody > tr:nth-child(2) > th').eq('5').should('contain', 'block')
        cy.get('div.col-md-7 > table > tbody > tr:nth-child(3) > th').eq('5').should('contain', 'block')
    })
})

describe('Scenario 10', () => {
    it('launch a new simulation of 200s', () => {
        //refresh page
        cy.visit('/')
        cy.get('a[href="#configuration"]').click();

        //test that there is no vehicles
        cy.get('div.col-md-8 > table').contains('No vehicle available');

        //add 3 vehicles
        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('50')
        cy.get('form[name="addVehicle"] >button').click()
        cy.get('div.modal-footer button').click();

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('80')
        cy.get('form[name="addVehicle"] >button').click()
        cy.get('div.modal-footer button').click();

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('80')
        cy.get('form[name="addVehicle"] >button').click()
        cy.get('div.modal-footer button').click();

        //launch the simulation
        cy.contains('Simulation').click()
        cy.get('input[name="time"][form="runNetwork"]').clear().type('200')
        cy.get('form[name="runNetwork"] >button').click()
        cy.wait(10000)

        // test step of vehicles
        cy.get('div.col-md-7 > table > tbody > tr:nth-child(1) > th').eq('4').should('contain', '29')
        cy.get('div.col-md-7 > table > tbody > tr:nth-child(2) > th').eq('4').should('contain', '29')
        cy.get('div.col-md-7 > table > tbody > tr:nth-child(3) > th').eq('4').should('contain', '17')
    })
})